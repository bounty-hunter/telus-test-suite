package com.telus.automation.suite.tc.web;

import static com.telus.automation.agent.selenium.web.ElementUtils.click;
import static com.telus.automation.agent.selenium.web.ElementUtils.input;
import static com.telus.automation.agent.selenium.web.ElementUtils.navigateToUrl;
import static com.telus.automation.agent.selenium.web.ElementUtils.setDriver;
import static com.telus.automation.suite.core.CommonConstants.APP_URL;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.telus.automation.agent.selenium.web.DriverPool;
import com.telus.automation.suite.core.TestDataRecorder;
import com.telus.automation.suite.modal.InputTestData;
import com.telus.automation.suite.tc.actions.HomePageActions;
import com.telus.automation.suite.tc.validators.HomePageValidator;

public class HomePage {
	
	private static ITestContext context;
	private WebDriver driver;
	private static final Logger LOGGER = LoggerFactory.getLogger(HomePage.class);
	public HomePageActions homePageActions;
	public HomePageValidator homePageValidator;
	
	@Parameters({"nodeURL"})
	@BeforeClass
	public void setConfig(ITestContext iTestContext ,String nodeURL) throws Exception {
		driver = DriverPool.getDriver("chrome", nodeURL);
		System.out.println("######  Driver is : " + driver);
		setDriver(driver);
		this.context = DriverPool.setContext(iTestContext, driver);
		homePageActions = new HomePageActions(driver);
		homePageValidator = new HomePageValidator();
	}
	
	@Test
	public void test_homepage() throws Exception {
		navigateToUrl(APP_URL);
		InputTestData testData = TestDataRecorder.getInputData("TEL_1");
		input("txtbx_streetAddress_name", testData.getStreetAddress());
		input("txtbx_zipCode_name", testData.getZipCode());
	    click("btn_view_all_offers_xpath");
	}
	
	@Test
	public void test_homepage1() throws Exception {
		navigateToUrl(APP_URL);
		InputTestData testData = TestDataRecorder.getInputData("TEL_2");
		input("txtbx_streetAddress_name", testData.getStreetAddress());
		input("txtbx_zipCode_name", testData.getZipCode());
	    click("btn_continue_id");
	}
	
	@AfterClass
	public void tearDown() {
		LOGGER.info("Closing andd killing browser instance");
/*		driver.close();
		driver.quit();*/
	}
}
