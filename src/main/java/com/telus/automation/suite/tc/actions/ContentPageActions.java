package com.telus.automation.suite.tc.actions;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContentPageActions {

	private WebDriver driver;
	
	public ContentPageActions(WebDriver driver) {
		this.driver = driver;
	}
	private static final Logger LOGGER = LoggerFactory.getLogger(ContentPageActions.class);
	
	/**
	 * It will select the menu item(select option) from the navigation bar present
	 * at header of HallWaze Page. Valid input Values: Post, Home, Groups, Tasks,
	 * Calender, Droplets, Notifications, Profile
	 * 
	 * @param menuItem
	 *            valid select option from menu
	 */
	public void selectFromHeaderMenu(String menuItem) {
		
		LOGGER.info("##########  Selecting {} option from header navigation menu", menuItem);
		
	}
}
