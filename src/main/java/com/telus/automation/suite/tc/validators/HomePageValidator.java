package com.telus.automation.suite.tc.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

public class HomePageValidator {

	private static final Logger LOGGER = LoggerFactory.getLogger(HomePageValidator.class);

	/**
	 * It will validate if the HomePage URL is as expected.
	 * 
	 * @param expectedURL
	 * @param actualURL
	 */
	public void validateHomePageURL(String actualURL, String expectedURL) {
		LOGGER.info("#############  Validating HomePageURL ####################################");
		Assert.assertEquals(actualURL, expectedURL);

	}
}
