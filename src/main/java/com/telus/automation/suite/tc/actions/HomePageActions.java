package com.telus.automation.suite.tc.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HomePageActions {

	private static final Logger LOGGER = LoggerFactory.getLogger(HomePageActions.class);
	private WebDriver driver;

	public HomePageActions(WebDriver driver) {
		this.driver = driver;
	}

	/** It will login in the hallwaze application.
	 * @param username
	 * @param password
	 */
	public void login(String username, String password) {
		LOGGER.info("##############  Signing In to Hallwaze #################");
		
		WebElement signInLink = driver.findElement(By.cssSelector("p a"));
		signInLink.click();
		WebElement email = driver.findElement(By.cssSelector("#authform input"));
		email.sendKeys(username);
		WebElement nextButton = driver.findElement(By.cssSelector("#authform button"));
		nextButton.click();
		WebElement passwd = driver.findElement(By.cssSelector("input[type='password']"));
		passwd.sendKeys(password);
		WebElement signInButton = driver.findElement(By.cssSelector("#authform > button"));
		signInButton.click();
	}
}
