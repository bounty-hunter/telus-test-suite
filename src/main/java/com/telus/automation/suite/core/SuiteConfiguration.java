package com.telus.automation.suite.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class SuiteConfiguration {
	private static final Logger LOGGER = LoggerFactory.getLogger(SuiteConfiguration.class);
	
	@BeforeSuite
	public void beforeSuiteConfiguration() {
		LOGGER.info("Automation test suite execution started");
		TestDataRecorder.recordTestData();
	}
	
	@AfterSuite
	public void afterSuiteConfiguration() {
	}
}
