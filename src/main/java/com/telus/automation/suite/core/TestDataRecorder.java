package com.telus.automation.suite.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.telus.automation.suite.modal.InputTestData;
import com.telus.automation.suite.modal.TCMapping;

/**
 * This class holds the responsibility of recording all the test data into cache
 * memory.
 * 
 * @author jaikant
 *
 */
public class TestDataRecorder {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TestDataRecorder.class);
	public static Map<String, TCMapping> TC_MAPPING = new HashMap<>();
	public static Map<String, InputTestData> TEST_DATA_CACHE = new HashMap<>();

	/**
	 * It will save the input test data in cache and that can be accessed across the
	 * application.
	 */
	public static void recordTestData() {
		String inputDataFileName = "inputTestData.xlsx";
		File inputDatafile = new File(System.getProperty("user.dir") + File.separatorChar + "src" + File.separatorChar
				+ "main" + File.separatorChar + "resources" + File.separatorChar + "testData" + File.separatorChar
				+ inputDataFileName);
		startRecording(inputDatafile);
	}

	/**
	 * It will give the required test data based on the testcase ID
	 * 
	 * @param testCaseId
	 * @return object of InputTestData
	 */
	public static InputTestData getInputData(String testCaseId) {
		LOGGER.info("#############  Get Input Test Data for TC-ID:{} ##########################", testCaseId);
		
		InputTestData inputData = TEST_DATA_CACHE.get(TC_MAPPING.get(testCaseId).getScenarioId());
		
		LOGGER.info("#############  Input Test Data for TC-ID:{} ##########################", inputData);
		
		return inputData;
	}

	private static void startRecording(File inputFile) {
		FileInputStream file;
		XSSFWorkbook workbook = null;
		try {
			file = new FileInputStream(inputFile);
			workbook = new XSSFWorkbook(file);
			recordTCMappings(workbook);
			recordScenario(workbook);
		} catch (Exception e) {
			LOGGER.info("Exception occured while recording data : {}",e.getMessage());
		} finally {
			try {
				workbook.close();
			} catch (IOException e) {
				LOGGER.info("Exception occured while recording data : {}",e.getMessage());
			}
		}
	}

	/**
	 * This function will record TC-MAPPING sheet data from inputdata.xlsx file
	 * 
	 * @param workbook
	 */
	private static void recordTCMappings(XSSFWorkbook workbook) {
		LOGGER.info("#############  Reading TC-MAPPING SHEET ##########################");
		XSSFSheet sheet = workbook.getSheet("TC-MAPPING");
		Iterator<Row> rowIterator = sheet.iterator();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			if (row.getRowNum() != 0) {
				String testCaseId = row.getCell(0).getStringCellValue();
				String scenarioId = row.getCell(1).getStringCellValue();
				TCMapping mapping = new TCMapping(testCaseId, scenarioId);
				TC_MAPPING.put(testCaseId, mapping);
			}
		}
	}

	/**
	 * It will record every scenario from the scenario sheet from inputdata.xlsx
	 * file
	 * 
	 * @param workbook
	 */
	private static void recordScenario(XSSFWorkbook workbook) {
		LOGGER.info("#############  Reading SCENARIO SHEET ##########################");
		XSSFSheet sheet = workbook.getSheet("SCENARIO");
		Iterator<Row> rowIterator = sheet.iterator();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			if (row.getRowNum() != 0) {
				String id = row.getCell(0).getStringCellValue();
				String streetAddress = row.getCell(1).getStringCellValue();
				String zipCode = row.getCell(2).getStringCellValue();
				InputTestData testData = new InputTestData(streetAddress, zipCode);
				TEST_DATA_CACHE.put(id, testData);
			}
		}

	}

}
