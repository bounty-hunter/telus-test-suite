package com.telus.automation.suite.modal;

public class InputTestData {

	private String streetAddress;
	private String zipCode;

	
	public InputTestData(String streetAddress, String zipCode) {
		this.streetAddress = streetAddress;
		this.zipCode = zipCode;
	}


	public String getStreetAddress() {
		return streetAddress;
	}


	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}


	public String getZipCode() {
		return zipCode;
	}


	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	@Override
	public String toString() {
		return "InputTestData [streetAddress=" + streetAddress + ", zipCode=" + zipCode + "]";
	}


	
}
