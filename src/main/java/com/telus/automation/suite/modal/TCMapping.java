package com.telus.automation.suite.modal;

public class TCMapping {

	private String testCaseId;
	private String scenarioId;
	
	public TCMapping(String testCaseId, String scenarioId) {
		this.testCaseId = testCaseId;
		this.scenarioId = scenarioId;
	}

	public String getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}

	public String getScenarioId() {
		return scenarioId;
	}

	public void setScenarioId(String scenarioId) {
		this.scenarioId = scenarioId;
	}

	@Override
	public String toString() {
		return "TCMapping [testCaseId=" + testCaseId + ", scenarioId=" + scenarioId + "]";
	}
}
